//+------------------------------------------------------------------+
//|                                                    CoreModel.mqh |
//|                                                    Man-of-Christ |
//|                                                                  |
//+------------------------------------------------------------------+
#property copyright "Man-of-Christ"
#property link      ""
#property version   "1.00"
#property strict

#ifndef COREMODEL_MQH
#define COREMODEL_MQH

#include "../Globals.mqh"
#include "../Helper/Status/StatusWriterFactory.mqh"

class CoreModel {
public:
  string m_symbol;
  int m_direction;

  double m_avgPrice;
  double m_level;
  
  double  m_buyer, m_seller;
  double m_profit;
  double m_point, m_size, m_take, m_spread;
  bool m_initpainel;
  string m_filters_on ;
  datetime m_time_equityrisk;

  double m_total_lots;
  int m_pos_count;

  IStatusWriter* mStatus;

public:
  void Status() {
    int MAGIC=InpMagic;
    double Avg_Price = m_avgPrice;
    int Dir = m_direction;
    double Profit_Last_Bar = m_profit;
    double Total_Lots = m_total_lots;
    int Pos_Count = m_pos_count;
    bool HTTP_Enabled = InpHTTPActivated;
    
    mStatus.INT(MAGIC);
    mStatus.DBL(Avg_Price);
    mStatus.DIR(Dir);
    mStatus.DBL(Profit_Last_Bar);
    mStatus.DBL(Total_Lots);
    mStatus.INT(Pos_Count);
    mStatus.TF(HTTP_Enabled);
  }

  CoreModel() {
    m_symbol = Symbol();
    m_point = 1.0 / MathPow(10, Digits);
    m_size = InpGridSize * m_point;
    m_take = InpTakeProfit * m_point;
    m_direction = 0;
    m_spread = 0.0;
    m_time_equityrisk = -1;
    m_filters_on = "";
    m_initpainel = true;
    mStatus = StatusWriterFactory::Singleton();
  }
};

#endif