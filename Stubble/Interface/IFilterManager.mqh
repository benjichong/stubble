//+------------------------------------------------------------------+
//|                                               IFilterManager.mqh |
//|                                                    Man-of-Christ |
//|                                                                  |
//+------------------------------------------------------------------+
#property copyright "Man-of-Christ"
#property link      ""
#property version   "1.00"
#property strict

#ifndef IFILTERMANAGER_MQH
#define IFILTERMANAGER_MQH

#include "IFilter.mqh"

interface IFilterManager {
  bool Triggered();
  void Insert(IFilter* filter);
};

#endif