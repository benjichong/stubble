//+------------------------------------------------------------------+
//|                                              IKillAtDrawDown.mqh |
//|                                                    Man-of-Christ |
//|                                                                  |
//+------------------------------------------------------------------+
#property copyright "Man-of-Christ"
#property link      ""
#property version   "1.00"
#property strict

#ifndef IKILLATDRAWDOWN_MQH
#define IKILLATDRAWDOWN_MQH

interface IKillAtDrawDown {
  void TryClosePositions(IPositions* positions);
};

#endif