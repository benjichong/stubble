//+------------------------------------------------------------------+
//|                                                ILotAllocator.mqh |
//|                                                    Man-of-Christ |
//|                                                                  |
//+------------------------------------------------------------------+
#property copyright "Man-of-Christ"
#property link      ""
#property version   "1.00"
#property strict

#ifndef ILOTALLOCATOR_MQH
#define ILOTALLOCATOR_MQH

interface ILotAllocator {
  double AllocateBaseLotsize();
  double Allocate(
    double inLots,
    int total_orders_count, // buy + sell counts
    bool long_condition,
    bool short_condition,
    double buyer_lots, 
    double seller_lots
  );
};

#endif