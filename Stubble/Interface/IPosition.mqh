//+------------------------------------------------------------------+
//|                                                    IPosition.mqh |
//|                                                    Man-of-Christ |
//|                                                                  |
//+------------------------------------------------------------------+
#property copyright "Man-of-Christ"
#property link      ""
#property version   "1.00"
#property strict

#ifndef IPOSITION_MQH
#define IPOSITION_MQH

interface IPosition {
  double PriceForClose();
  double PriceForOpen();
  double OpenPrice();
  double SLPrice(double iSlDist);
  double TPPrice(double iTpDist);
  IPosition* AdjSL(double iSLDist);
  IPosition* AdjSLToPrice(double iSLPrice);
  IPosition* AdjTP(double iTPDist);
  IPosition* AdjTPToPrice(double iTPPrice);
  IPosition* RemoveSL();
  IPosition* SLToDistAwayFromOpen(const double d);
  bool TrailSLReq(double iSLDist);
  string Descrip();
  void Close();
  datetime DiffFrOpenTime();
  void Status(string x);
};

#endif