//+------------------------------------------------------------------+
//|                                                   IPositions.mqh |
//|                                                    Man-of-Christ |
//|                                                                  |
//+------------------------------------------------------------------+
#property copyright "Man-of-Christ"
#property link      ""
#property version   "1.00"
#property strict

#ifndef IPOSITIONS_MQH
#define IPOSITIONS_MQH

#include "../Globals.mqh"
#include "IPosition.mqh"

interface IPositions {
  IPosition* Add(int iType, double iLot, double iSLDist=0, string iMsg="");
  IPosition* Add(int iType, double iLot, double iPrice, double iSLDist=0, string iMsg="");
  void CloseAndRemoveAll();
  int Refresh();
  int GetAllPositions(int type);
  int GetAllPositions(int type, IPositions* p);
  int TradeDirectionOfLastClosed();
  double Profit();
  bool OnTrailingStop(double iSLDist);
  void RemoveSL();
  void AdjSLToPrice(const double d);
  void AdjTPToPrice(const double d);
  double TotalLots();
  double Sum(double spread);
  IPosition* At(int idx);
  int Count();
  void Sort();
};

#endif