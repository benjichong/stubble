//+------------------------------------------------------------------+
//|                                                IStatusWriter.mqh |
//|                                                    Man-of-Christ |
//|                                                                  |
//+------------------------------------------------------------------+
#property copyright "Man-of-Christ"
#property link      ""
#property version   "1.00"
#property strict

#ifndef ISTATUSWRITER_MQH
#define ISTATUSWRITER_MQH

#include "../Helper/StringSettingsHelper.mqh"

#define TF(s) Status(#s, CStringSettingHelper::TrueFalse(s))
#define TYPE(s) Status(#s, CStringSettingHelper::TypeToStr(s))
#define DBL(s) Status(#s, DoubleToStr(s,4))
#define INT(s) Status(#s, IntegerToString(s))
#define STR(s) Status(#s, s)
#define ENUM(s) Status(#s, EnumToString(s))
#define OTYP(s) Status(#s, (s==OP_BUY?"BUY":"SELL"))
#define DIR(s) Status(#s, (s>0?"BUY":s<0?"SELL":""))

interface IStatusWriter {
  void Status(string key, string value);
  void Trace(string x);
  void Debug(string x);
  void Render();
};

#endif