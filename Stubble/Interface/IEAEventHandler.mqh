//+------------------------------------------------------------------+
//|                                              IEAEventHandler.mqh |
//|                                                    Man-of-Christ |
//|                                                                  |
//+------------------------------------------------------------------+
#property copyright "Man-of-Christ"
#property link      ""
#property version   "1.00"
#property strict

#ifndef IEAEVENTHANDLER_MQH
#define IEAEVENTHANDLER_MQH

#include "../Globals.mqh"

interface IEAEventHandler {
  void OnTick();
  void OnTimer();
};

#endif