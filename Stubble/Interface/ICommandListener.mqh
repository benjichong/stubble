//+------------------------------------------------------------------+
//|                                             ICommandListener.mqh |
//|                                                    Man-of-Christ |
//|                                                                  |
//+------------------------------------------------------------------+
#property copyright "Man-of-Christ"
#property link      ""
#property version   "1.00"
#property strict

#ifndef ICOMMANDLISTENER_MQH
#define ICOMMANDLISTENER_MQH

#include "../Globals.mqh"
#include "IKillAtDrawDown.mqh"
#include "IFilter.mqh"

interface ICommandListener : public IKillAtDrawDown {
  void OnTick();
  bool NoNewTrades();
  bool CloseAllPositions();
  double TPRequested();
  void TryAdjTP(IPositions* positions);
  IFilter* GetFilter();
};

#endif