//+------------------------------------------------------------------+
//|                                                      IFilter.mqh |
//|                                                    Man-of-Christ |
//|                                                                  |
//+------------------------------------------------------------------+
#property copyright "Man-of-Christ"
#property link      ""
#property version   "1.00"
#property strict

#ifndef IFILTER_MQH
#define IFILTER_MQH

interface IFilter {
  bool Triggered();
};

#endif