//+------------------------------------------------------------------+
//|                                              ISignalProvider.mqh |
//|                                                    Man-of-Christ |
//|                                                                  |
//+------------------------------------------------------------------+
#property copyright "Man-of-Christ"
#property link      ""
#property version   "1.00"
#property strict

#ifndef ISIGNALPROVIDER_MQH
#define ISIGNALPROVIDER_MQH

interface ISignalProvider {
  bool BuySignal();
  bool SellSignal();
};

#endif