//+------------------------------------------------------------------+
//|                                                      Globals.mqh |
//|                                                    Man-of-Christ |
//|                                                                  |
//+------------------------------------------------------------------+
#property copyright "Man-of-Christ"
#property link      ""
#property version   "1.00"
#property strict

#ifndef GLOBALS_MQH
#define GLOBALS_MQH

enum ENUM_LOT_MODE {
    LOT_MODE_FIXED = 1, // Fixed Lot
    LOT_MODE_PERCENT = 2, // Percent Lot
};

//--- input parameters
extern string               Version__ =  "----------------------------------------------------------------";
extern string               Version1__ = "-------------------- Stubble -----------------------------------";
extern string               Version2__ = "----------------------------------------------------------------";

extern string Magic = "--------Magic Number---------";
input int                   InpMagic= 7799;                // Magic Number  

extern string SignalConfig__ = "-----------------------------Signal-----------------------";
extern string MovingAverageConfig__ = "-----------------------------Moving Average-----------------------";
input ENUM_TIMEFRAMES        InpMaFrame= PERIOD_CURRENT;      // Moving Average TimeFrame
input int                  InpMaPeriod= 3;                    // Moving Average Period
input ENUM_MA_METHOD         InpMaMethod= MODE_EMA;           // Moving Average Method
input ENUM_APPLIED_PRICE     InpMaPrice= PRICE_OPEN;          // Moving Average Price
input int                  InpMaShift= 0;                     // Moving Average Shift

extern string Config__ = "---------------------------Grid Config--------------------------------------";
input ENUM_LOT_MODE          InpLotMode= LOT_MODE_PERCENT;    // Lot Mode
input int                  InpGridSize= 30;                // Step Size in Points
input int                  InpTakeProfit= 30;              // Take Profit in Points
input double               InpFixedLot= 0.01;             // Fixed Lot
input double               InpPercentLot= 0.03;           // Percent Lot
input double               InpGridFactor= 1.3;            // Grid Increment Factor

extern string FilterOpenOneCandle__ = "--------------------Filter One Order by Candle--------------";
input bool                 InpOpenOneCandle = true;          // Open one order by candle
input ENUM_TIMEFRAMES        InpTimeframeBarOpen = PERIOD_CURRENT; // Timeframe OpenOneCandle

extern string FilterSpread__ = "----------------------------Filter Max Spread--------------------";
input int                  InpMaxSpread = 16;               // Max Spread 

extern string DrawDonwKill__ = "----------------------------Kill @ Drawdown--------------------";
input double               InpMaxDrawdown = 750;            // Max Drawdown in your currency 

extern string EquityCaution__ = "------------------------Filter Caution of Equity ---------------";
extern bool                 InpUseEquityCaution = TRUE;            //  EquityCaution?
extern double               InpTotalEquityRiskCaution = 20;        // Total % Risk to EquityCaution
extern ENUM_TIMEFRAMES        InpTimeframeEquityCaution = PERIOD_D1; // Timeframe as EquityCaution

input string CoomunicatorSettings__ = "-------------------------HTTP Settings---------------------------";
extern bool                 InpHTTPActivated = FALSE; // Toggle HTTP ON/OFF

int SlipPage= 3;
int Spread = 2.0; 

string DuralURL = "http://localhost:3434";

#endif