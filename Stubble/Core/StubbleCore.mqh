//+------------------------------------------------------------------+
//|                                                StubbleCore.mqh |
//|                                                    Man-of-Christ |
//|                                                                  |
//+------------------------------------------------------------------+
#property copyright "Man-of-Christ"
#property link      ""
#property version   "1.00"
#property strict

#ifndef STUBBLE_MQH
#define STUBBLE_MQH

#include "../Globals.mqh"
#include "../Interface/IEAEventHandler.mqh"
#include "../Interface/IPositions.mqh"
#include "../Interface/ISignalProvider.mqh"
#include "../Interface/IFilterManager.mqh"
#include "../Interface/IFilter.mqh"
#include "../Interface/ILotAllocator.mqh"
#include "../Interface/ICommandListener.mqh"
#include "../Interface/IKillAtDrawDown.mqh"
#include "../Helper/Positions/PositionsWrapper1.mqh"
#include "../Helper/SignalProvider/MASignalProvider.mqh"
#include "../Helper/Filter/FilterManager.mqh"
#include "../Helper/KillAtDrawDown/KillAtDrawDown.mqh"
#include "../Helper/CommandListener/CommandListener.mqh"
#include "../Helper/LotAllocator.mqh"
#include "../Helper/Positions/AggregatedLevelHelper.mqh"
#include "../Model/CoreModel.mqh"

#define TSTUBBLECORE StubbleCore

class StubbleCore : public IEAEventHandler {
private:
  CoreModel mCoreModel;
private:
  AutoDeletor<ISignalProvider> mSignalProvider;
  AutoDeletor<IPositions> mPositions;
  AutoDeletor<IFilterManager> mFilterManager;
  AutoDeletor<ILotAllocator> mLotAllocator;
  AutoDeletor<ICommandListener> mCommandListener;
  AutoDeletor<IKillAtDrawDown> mKillAtDrawDown;
private:
  StubbleCore(
    ISignalProvider* signalProvider,
    IPositions* positions,
    IFilterManager* filtermanager,
    ILotAllocator* lotAllocator,
    ICommandListener* commandListener,
    IKillAtDrawDown* killAtDrawDown
  ) :
    mCoreModel(),
    mSignalProvider(signalProvider),
    mPositions(positions),
    mFilterManager(filtermanager),
    mLotAllocator(lotAllocator),
    mCommandListener(commandListener),
    mKillAtDrawDown(killAtDrawDown)
  {
    filtermanager.Insert(commandListener.GetFilter());
    mCoreModel.Status();
  }

private:
  static StubbleCore* Factory() {
    ISignalProvider* sp = new MASignalProvider();
    IPositions* p = PositionsWapper1Factory::New();
    IFilterManager* f = new FilterManager();
    ILotAllocator* l = new LotAllocator();
    ICommandListener* c = new CommandListener();
    IKillAtDrawDown* k = new KillAtDrawDown();

    return new StubbleCore(sp, p, f, l, c, k);
  }
public:
  static StubbleCore* Ref() {
    static AutoDeletor<StubbleCore> n(Factory());
    return n.Ref();
  }

protected:
  void CheckTradeCondition(
    const int buy_count, 
    const int sell_count,
    IPositions* buyPositions,
    IPositions* sellPositions, 
    bool& long_condition, 
    bool& short_condition
  ) {
    long_condition = false;
    short_condition = false;

    if (buy_count > 0 || sell_count > 0) {
      // add action here
    }
    else
    if (mSignalProvider.Ref().BuySignal()) {
      mCoreModel.mStatus.Trace("New Buy Cycle.");
      long_condition = true;
    }
    else
    if (mSignalProvider.Ref().SellSignal()) {
      mCoreModel.mStatus.Trace("New Sell Cycle.");
      short_condition = true;
    } 
  }

  void ConditionalSetEquityAlert() {
    // CONTROL DRAWDOWN
    double vProfit = mPositions.Ref().Profit();
    if (vProfit < 0. && MathAbs(vProfit) > InpTotalEquityRiskCaution / 100.0 * AccountEquity()) {
        // mCoreModel.mStatus.Debug("time_equityrisk warning is on");
        mCoreModel.m_time_equityrisk = iTime(NULL, InpTimeframeEquityCaution, 0);
    } else { 
      // mCoreModel.mStatus.Debug("time_equityrisk warning is off");
      mCoreModel.m_time_equityrisk = -1; 
    } 
  }

  void ConditionalRaiseEquityAlert() {
    if (mCoreModel.m_time_equityrisk <= iTime(NULL, InpTimeframeEquityCaution, 0)) {
      string alert = "Equity Caution Alert!";
    }
  }

public:
  void OnTick() {
    mCommandListener.Ref().OnTick();
    mCoreModel.m_spread = MarketInfo(Symbol(), MODE_SPREAD) * mCoreModel.m_point;
    if (mPositions.Ref().Refresh() <= 0) { 
      mCoreModel.m_time_equityrisk = -1;
    }
    mKillAtDrawDown.Ref().TryClosePositions(mPositions.Ref());
    mCommandListener.Ref().TryClosePositions(mPositions.Ref());
    mCommandListener.Ref().TryAdjTP(mPositions.Ref());

    ConditionalRaiseEquityAlert();
    if (mFilterManager.Ref().Triggered()) return;

    AutoDeletor<IPositions> buyPositions(PositionsWapper1Factory::New());
    AutoDeletor<IPositions> sellPositions(PositionsWapper1Factory::New());
    int buy_count = 0;
    int sell_count = 0;
    bool long_condition = false;
    bool short_condition = false;
    double level = 0.;
    double close_price= iClose(NULL, 0, 0);
    double open_price= iOpen(NULL, 0, 0);
    bool was_trade = false;
    bool close_filter = false;

    // Base lot size
    double lots = mLotAllocator.Ref().AllocateBaseLotsize();

    // DailyTargetRefresh();
    buyPositions.Ref().GetAllPositions(OP_BUY, mPositions.Ref());
    sellPositions.Ref().GetAllPositions(OP_SELL, mPositions.Ref());
    mCoreModel.m_profit = mPositions.Ref().Profit();
    // orders_count = mPositions.Ref().Count();
    buy_count = buyPositions.Ref().Count();
    sell_count = sellPositions.Ref().Count();
    buyPositions.Ref().Sort();
    sellPositions.Ref().Sort();

    CheckTradeCondition(
      buy_count, 
      sell_count, 
      buyPositions.Ref(), 
      sellPositions.Ref(), 
      long_condition, 
      short_condition
    );
    
    ConditionalSetEquityAlert();

    double buyer_lots = buyPositions.Ref().TotalLots();
    double seller_lots = sellPositions.Ref().TotalLots();
    lots = mLotAllocator.Ref().Allocate(
      lots,
      buy_count + sell_count,
      long_condition,
      short_condition,
      buyer_lots,
      seller_lots
    );
    
    if (long_condition) {
      mCoreModel.mStatus.Trace("BUY at " + DBLSTRX(Ask));
      IPosition* p = mPositions.Ref().Add(OP_BUY, lots, 0, "BUY");
      if (p) {
          buyPositions.Ref().GetAllPositions(OP_BUY, mPositions.Ref());
          buyPositions.Ref().Sort();
          mCoreModel.m_avgPrice = AggregateLevelHelper::BuyOpenPrice(
            buyPositions.Ref(), 
            sellPositions.Ref(), 
            mCoreModel.m_spread
          );
          mCoreModel.m_level = mCoreModel.m_avgPrice + mCoreModel.m_take;
          level = mCoreModel.m_level;
          if (buyPositions.Ref().Count() == 1) {
              // this is a first buy
              mCoreModel.m_buyer = p.OpenPrice();
          }
          mCoreModel.m_direction = 1;
          was_trade = true;
      }
    }

    if (short_condition) {
      mCoreModel.mStatus.Trace("SELL at " + DBLSTRX(Bid));
      IPosition* p = mPositions.Ref().Add(OP_SELL, lots, 0, "SELL");
      if (p) {
        sellPositions.Ref().GetAllPositions(OP_SELL, mPositions.Ref());
        sellPositions.Ref().Sort();
        mCoreModel.m_avgPrice = AggregateLevelHelper::SellOpenPrice(
          buyPositions.Ref(), 
          sellPositions.Ref(), 
          mCoreModel.m_spread
        );
        mCoreModel.m_level = mCoreModel.m_avgPrice - mCoreModel.m_take;
        level = mCoreModel.m_level;
        if (sellPositions.Ref().Count() == 1) {
          // this is a first sell
          mCoreModel.m_seller = p.OpenPrice();
        }
        mCoreModel.m_direction = -1;
        was_trade = true;
      }
    }

    //--- Setup Global TP
    if (was_trade) {
        // mCoreModel.mStatus.Trace("setting up global TP.");
        mPositions.Ref().AdjTPToPrice(level);
    }

    mCoreModel.m_total_lots = mPositions.Ref().TotalLots();
    mCoreModel.m_pos_count = mPositions.Ref().Count();
    mCoreModel.Status();
    mCoreModel.mStatus.Render();
  }

  void OnTimer() {
    // add action here.
  }
};

#endif