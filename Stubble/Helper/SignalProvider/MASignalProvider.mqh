//+------------------------------------------------------------------+
//|                                             MASignalProvider.mqh |
//|                                                    Man-of-Christ |
//|                                                                  |
//+------------------------------------------------------------------+
#property copyright "Man-of-Christ"
#property link      ""
#property version   "1.00"
#property strict

#ifndef MASIGNALPROVIDER_MQH
#define MASIGNALPROVIDER_MQH

#include "../../Globals.mqh"
#include "../../Interface/ISignalProvider.mqh"
#include "../Status/StatusWriterFactory.mqh"

class MASignalProvider : public ISignalProvider {
private:
  IStatusWriter* mStatus;
public:
  bool BuySignal() {
    // string iclose = DoubleToStr(iClose(NULL, 0, 0));
    // string ima = DoubleToStr(iMA(NULL, InpMaFrame, InpMaPeriod, 0, InpMaMethod, InpMaPrice, InpMaShift));
    // mStatus.Trace(iclose + " > " + ima);
    return iClose(NULL, 0, 0) > iMA(NULL, InpMaFrame, InpMaPeriod, 0, InpMaMethod, InpMaPrice, InpMaShift);
  }

  bool SellSignal() {
    // string iclose = DoubleToStr(iClose(NULL, 0, 0));
    // string ima = DoubleToStr(iMA(NULL, InpMaFrame, InpMaPeriod, 0, InpMaMethod, InpMaPrice, InpMaShift));
    // mStatus.Trace(iclose + " < " + ima);
    return iClose(NULL, 0, 0) < iMA(NULL, InpMaFrame, InpMaPeriod, 0, InpMaMethod, InpMaPrice, InpMaShift);
  }

  MASignalProvider() :
    mStatus(StatusWriterFactory::Singleton())
  {
  }
};

#endif