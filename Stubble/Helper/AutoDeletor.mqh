//+------------------------------------------------------------------+
//|                                                  AutoDeletor.mqh |
//|                                                    Man-of-Christ |
//|                                                                  |
//+------------------------------------------------------------------+
#property copyright "Man-of-Christ"
#property link      ""
#property version   "1.00"
#property strict

#ifndef AUTODELETOR_MQH
#define AUTODELETOR_MQH

template<typename T>
class AutoDeletor {
    T* m_T;
public:
    AutoDeletor(T* inT) {
        m_T = inT;
    }
    ~AutoDeletor() {
        delete m_T;
    }

    T* Ref() {
        return m_T;
    }
};

#endif