//+------------------------------------------------------------------+
//|                                              CommandListener.mqh |
//|                                                    Man-of-Christ |
//|                                                                  |
//+------------------------------------------------------------------+
#property copyright "Man-of-Christ"
#property link      ""
#property version   "1.00"
#property strict

#ifndef COMMANDLISTENER_MQH
#define COMMANDLISTENER_MQH

#include "../../Globals.mqh"
#include "../../Interface/ICommandListener.mqh"
#include "../../Interface/IFilter.mqh"
#include "../DLLInterfacer/HttpClient.mqh"

#define RESPONSE_ERROR "Error"
#define NONEWTRADES "nonewtrades"
#define CLOSEALLTRADES "closealltrades"
#define RESUMETRADES "resumetrades"
#define ADJTP "adjtp"

class CommandListener;
class CommandLineFilter: public IFilter {
private:
  CommandListener* _self;
public:
  CommandLineFilter(CommandListener* self): 
    _self(self) 
  {
  }

  bool Triggered() {
    return _self.NoNewTrades();
  }
};

class CommandListener: public ICommandListener {
private:
  IStatusWriter* _status;
  bool _noNewTrades, _closeAllPositions;
  double _newTPPosition;

private:
  bool IsValidResponse(string& response) {
    return StringLen(response) > 0 && StringFind(response, RESPONSE_ERROR, 0) <= -1;
  }

  bool SetCommandState(const string found, string command, bool& state) {
    if (StringFind(found, command + ":", 0) == 0) {
      int v = StringLen(command) + 1;
      string val = StringSubstr(found, v, StringLen(found) - v);
      state = (val == "TRUE") ? true : false;
      // _status.Trace("SetCommandState B activated");
      return true;
    }
    return false;
  }

  bool SetCommandState(const string found, string command, double& state) {
    if (StringFind(found, command + ":", 0) == 0) {
      int v = StringLen(command) + 1;
      string val = StringSubstr(found, v, StringLen(found) - v);
      state = (val == "FALSE") ? -1. : StringToDouble(val);
      // _status.Trace("SetCommandState D activated");
      return true;
    }
    return false;
  }

  void SetAllCommands(const string& found) {
    if (SetCommandState(found, CLOSEALLTRADES, _closeAllPositions)) return;
    bool setNoNewTrades = false;
    if (SetCommandState(found, NONEWTRADES, setNoNewTrades)) {
      if (setNoNewTrades) _noNewTrades = true;
    }
    bool setResumeTrade = false;
    if (SetCommandState(found, RESUMETRADES, setResumeTrade)) {
      if (setResumeTrade) _noNewTrades = false;
      return;
    }
    if (setNoNewTrades || setResumeTrade) {
      _status.TF(_noNewTrades);
      _status.Render();
    }
    SetCommandState(found, ADJTP, _newTPPosition);
  }

  void ParseResponse(string& response) {
    int idx0=0, previdx0=0;
    string found = "";
    for (; idx0 > -1; idx0 = StringFind(response, "#", idx0)) {
      if (idx0 <= 0) continue;
      
      found = StringSubstr(response, previdx0, idx0-previdx0);

      // _status.Trace("ParseResponse found " + found);
      // _status.Render();
      SetAllCommands(found);

      idx0++;
      previdx0 = idx0;
    }
    found = StringSubstr(response, previdx0, StringLen(response)-previdx0); 
    // _status.Trace("ParseResponse found " + found);
    // _status.Render();
    SetAllCommands(found);
  }

public:
  void OnTick() {
    if (!InpHTTPActivated) return;

    string magicStr = IntegerToString(InpMagic);

    string response = "";
    response = HttpClient::Get(DuralURL + "/allcommands/" + magicStr, "");
    if (IsValidResponse(response)) {
      // _status.Trace("wtf " + response);
      ParseResponse(response);
    }

    _status.TF(_closeAllPositions);
    _status.TF(_noNewTrades);
    _status.DBL(_newTPPosition);
  }

  bool NoNewTrades() {
    return _noNewTrades;
  }

  bool CloseAllPositions() {
    return _closeAllPositions;
  }

  double TPRequested() {
    return _newTPPosition;
  }

  void TryClosePositions(IPositions* positions) {
    if (_closeAllPositions) {
      _status.Trace("========================================================");
      _status.Trace("CLOSEALLPOSITION RECEIVED, POSITIONS KILLED ALL POSITION");
      _status.Trace("========================================================");
      positions.CloseAndRemoveAll();
      _closeAllPositions = false;
      _status.TF(_closeAllPositions);
    }
  }

  void TryAdjTP(IPositions* positions) {
    if (_newTPPosition > 0.) {
      _status.Trace("========================================================");
      _status.Trace("ADJTP RECEIVED, TPS ADJUSTED TO " + DoubleToString(_newTPPosition));
      _status.Trace("========================================================");
      positions.AdjTPToPrice(_newTPPosition);
      _newTPPosition = -1.;
      _status.DBL(_newTPPosition);
    }
  }

  IFilter* GetFilter() {
    return new CommandLineFilter(GetPointer(this));
  }

  CommandListener() :
    _status(StatusWriterFactory::Singleton()),
    _noNewTrades(false),
    _closeAllPositions(false),
    _newTPPosition(-1.)
  {
  }
};

#endif