//+------------------------------------------------------------------+
//|                                                  QuickSorter.mqh |
//|                                                    Man-of-Christ |
//|                                                                  |
//+------------------------------------------------------------------+
#property copyright "Man-of-Christ"
#property link      ""
#property version   "1.00"
#property strict

#ifndef QUICKSORTER_MQH
#define QUICKSORTER_MQH

template<typename ComparatorT, typename T, typename deletable>
class QuickSorter: public ComparatorT {
public:
    static void Sort(LinkedList<T, deletable>* arr, int low, int high) {
        if (low >= high) return;
        int pi = Partition(arr, low, high);
        Sort(arr, low, pi-1);
        Sort(arr, pi+1, high);
    }
private:
    static int Partition(LinkedList<T, deletable>* arr, int low, int high) {
        T pivot = arr[high];
        int i = low - 1;
        for (int j=low; j<= high-1; ++j) {
            // Greater is inherited from ComparatorT
            if (Greater(arr[j], pivot)) continue;
            ++i;
            Swap(arr, i, j);
        }
        Swap(arr, i+1, high);
        return i+1;
    }

    static void Swap(LinkedList<T, deletable>* arr, int a, int b) {
        T t = arr[a];
        arr.SwapAt(a, arr[b]);
        arr.SwapAt(b, t);
    }
};

#endif