//+------------------------------------------------------------------+
//|                                                    Positions.mqh |
//|                                                    Man-of-Christ |
//|                                                                  |
//+------------------------------------------------------------------+
#property copyright "Man-of-Christ"
#property link      ""
#property version   "1.00"
#property strict

#ifndef POSITIONS_MQH
#define POSITIONS_MQH

#include "../AutoDeletor.mqh"
#include "../LinkedList.mqh"
#include "../QuickSorter.mqh"
#include "Position.mqh"
#include "../Status/StatusWriterFactory.mqh"

class PositionComparator {
public:
    static bool Greater(Position* a, Position* b) {
        return a._ticket > b._ticket;
    } 
};

#define TPOSITIONSORTER QuickSorter<PositionComparator, Position*, Deletable<Position*>>
#define TPOSITIONLIST LinkedList<Position*, Deletable<Position*>>
#define TPOSITIONITER ListIterator<Position*, Deletable<Position*>>

class Positions : public TPOSITIONLIST {
    int mMagic, mSlippage;
    IStatusWriter* mStatus;
public:
    Positions(int iMagic, int iSlippage=20) : 
      mMagic(iMagic), mSlippage(iSlippage),
      mStatus(StatusWriterFactory::Singleton()) 
    {};
    ~Positions() {
    }

    Position* Add(int iType, double iLot, double iSLDist=0, string iMsg="") {
        return Add(new Position(mMagic, iType, iLot, mSlippage, iSLDist, iMsg));
    };

    Position* Add(int iType, double iLot, double iPrice, double iSLDist=0, string iMsg="") {
        return Add(new Position(mMagic, iType, iLot, iPrice, mSlippage, iSLDist, iMsg));
    };

    void CloseAndRemoveAll() {
        if (Count()<=0) {
            return;
        }
        for(
            AutoDeletor<TPOSITIONITER> iterator(GetIterator()); 
            !iterator.Ref().End(); 
            iterator.Ref().Next()
        ) {
            iterator.Ref().Current().Close();
        }
        DeleteAll();
    }

    int Refresh() {
        DeleteAll();
        int tot = OrdersTotal();
        for(int i=0; i<tot; i++) {
            bool x = OrderSelect(i, SELECT_BY_POS, MODE_TRADES);
            if (OrderSymbol()!=Symbol() || OrderMagicNumber()!=mMagic) continue;
            Add(new Position());
        }
        int ret = Count();
        return ret;
    }

    int GetAllPositions(int type) {
        DeleteAll();
        int tot = OrdersTotal();
        for(int i=0; i<tot; i++) {
            bool x = OrderSelect(i, SELECT_BY_POS, MODE_TRADES);
            if (OrderSymbol()!=Symbol() || OrderMagicNumber()!=mMagic || OrderType()!=type) continue;
            Add(new Position());
        }
        int ret = Count();
        string xx = __FUNCTION__ + " " + INTSTRX(ret);
        mStatus.Trace(xx);
        return ret;
    }

    int GetAllPositions(int type, Positions* p) {
        if (p.Count()<=0) {
            return 0;
        }
        DeleteAll();
        for(
            AutoDeletor<TPOSITIONITER> iterator(p.GetIterator()); 
            !iterator.Ref().End(); 
            iterator.Ref().Next()
        ) {
            if (iterator.Ref().Current()._type == type) {
                Add(new Position(iterator.Ref().Current()));
            }
        }
        int ret = Count();
        return ret;
    }

    int TradeDirectionOfLastClosed() {
        int t = OrdersHistoryTotal();
        int type;
        datetime closeTime;
        for (--t; t>=0; --t) {
            bool x = OrderSelect( t, SELECT_BY_POS, MODE_HISTORY);
            if (!x) continue;
            type = OrderType();
            closeTime = OrderCloseTime();
            if ((type==OP_BUY || type==OP_SELL) && closeTime > 0) {
                string xx = __FUNCTION__ + " " + OTYPSTRX(type);
                mStatus.Trace(xx);
                return type;
            }
        }
        string xx = __FUNCTION__ + " " + "NO TYPE";
        mStatus.Trace(xx);
        return EMPTY_VALUE;
    }

    double Profit() {
        double ret = 0.;
        if (Count() <= 0) {
            return ret;
        }
        for(
            AutoDeletor<TPOSITIONITER> iterator(GetIterator()); 
            !iterator.Ref().End(); 
            iterator.Ref().Next()
        ) {
            ret += iterator.Ref().Current()._profit;
        }
        return ret;
    }

    bool OnTrailingStop(double iSLDist) {
        bool ret = false;
        if (Count() <= 0) {
            return ret;
        }
        for(
            AutoDeletor<TPOSITIONITER> iterator(GetIterator()); 
            !iterator.Ref().End(); 
            iterator.Ref().Next()
        ) {
            if (iterator.Ref().Current().TrailSLReq(iSLDist)) {
                iterator.Ref().Current().AdjSL(iSLDist);
                ret = true;
            }
        }

        return ret;
    }

    void RemoveSL() {
        if (Count() <= 0) {
            return;
        }
        for(
            AutoDeletor<TPOSITIONITER> iterator(GetIterator()); 
            !iterator.Ref().End(); 
            iterator.Ref().Next()
        ) {
            if (iterator.Ref().Current()._sl>0.) {
                iterator.Ref().Current().RemoveSL();
            }
        }
    }

    void AdjSLToPrice(const double d) {
        if (Count() <= 0) {
            return;
        }
        for(
            AutoDeletor<TPOSITIONITER> iterator(GetIterator()); 
            !iterator.Ref().End(); 
            iterator.Ref().Next()
        ) {
            iterator.Ref().Current().AdjSLToPrice(d);
        }
    }

    void AdjTPToPrice(const double d) {
        if (Count() <= 0) {
            return;
        }
        for(
            AutoDeletor<TPOSITIONITER> iterator(GetIterator()); 
            !iterator.Ref().End(); 
            iterator.Ref().Next()
        ) {
            iterator.Ref().Current().AdjTPToPrice(d);
        }
    }

    double TotalLots() {
        double lots = 0.;
        if (Count() <= 0) {
            return lots;
        }
        for (
            AutoDeletor<TPOSITIONITER> iterator(GetIterator());
            !iterator.Ref().End();
            iterator.Ref().Next()
        ) {
            lots += iterator.Ref().Current()._lot;
        }
        return lots;
    }

    double Sum(double spread) {
        double sum = 0.;
        if (Count() <= 0) {
            return sum;
        }
        
        for (
            AutoDeletor<TPOSITIONITER> iterator(GetIterator());
            !iterator.Ref().End();
            iterator.Ref().Next()
        ) {
            
            sum += ((iterator.Ref().Current()._op + spread) * iterator.Ref().Current()._lot);
        }
        return sum;
    }
};

#endif