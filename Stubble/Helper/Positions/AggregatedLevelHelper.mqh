//+------------------------------------------------------------------+
//|                                        AggregatedLevelHelper.mqh |
//|                                                    Man-of-Christ |
//|                                                                  |
//+------------------------------------------------------------------+
#property copyright "Man-of-Christ"
#property link      ""
#property version   "1.00"
#property strict

#ifndef AGGREGATEDLEVELHELPER_MQH
#define AGGREGATEDLEVELHELPER_MQH

#include "../../Interface/IPositions.mqh";

class AggregateLevelHelper {
public:
  /// Calculate the "aggregated open price" given
  /// a list of positions.
  static double BuyOpenPrice(
    IPositions* buyPositions, 
    IPositions* sellPositions, 
    double spread
  ) {
    return NormalizeDouble(
      (buyPositions.Sum(spread) - sellPositions.Sum(spread)) / (buyPositions.TotalLots() - sellPositions.TotalLots()),
      Digits
    );
  }

  static double SellOpenPrice(
    IPositions* buyPositions, 
    IPositions* sellPositions, 
    double spread
  ) {
    return NormalizeDouble(
      (sellPositions.Sum(spread) - buyPositions.Sum(spread)) / (sellPositions.TotalLots() - buyPositions.TotalLots()),
      Digits
    );
  }
};

#endif