//+------------------------------------------------------------------+
//|                                            PositionsWrapper1.mqh |
//|                                                    Man-of-Christ |
//|                                                                  |
//+------------------------------------------------------------------+
#property copyright "Man-of-Christ"
#property link      ""
#property version   "1.00"
#property strict

#ifndef POSITIONSWRAPPER1_MQH
#define POSITIONSWRAPPER1_MQH

#include "../../Globals.mqh"
#include "../../Interface/IPositions.mqh";
#include "Positions.mqh";

class PositionsWapper1 : public IPositions {
private:
  Positions mPositions;
public:
  PositionsWapper1(int magicNumber, int slippage) :
    mPositions(magicNumber, slippage) 
  {}

public:
  IPosition* Add(int iType, double iLot, double iSLDist=0, string iMsg="") {
    return mPositions.Add(iType, iLot, iSLDist, iMsg);
  }

  IPosition* Add(int iType, double iLot, double iPrice, double iSLDist=0, string iMsg="") {
    return mPositions.Add(iType, iLot, iPrice, iSLDist, iMsg);
  };

  void CloseAndRemoveAll() {
    mPositions.CloseAndRemoveAll();
  }

  int Refresh() {
    return mPositions.Refresh();
  }

  int GetAllPositions(int type) {
    return mPositions.GetAllPositions(type);
  }

  int GetAllPositions(int type, IPositions* p) {
    return mPositions.GetAllPositions(type, GetPointer(((PositionsWapper1*)p).mPositions));
  }

  int TradeDirectionOfLastClosed() {
    return mPositions.TradeDirectionOfLastClosed();
  }

  double Profit() {
    return mPositions.Profit();
  }

  bool OnTrailingStop(double iSLDist) {
    return mPositions.OnTrailingStop(iSLDist);
  }

  void RemoveSL() {
    mPositions.RemoveSL();
  }

  void AdjSLToPrice(const double d) {
    mPositions.AdjSLToPrice(d);
  }

  void AdjTPToPrice(const double d) {
    mPositions.AdjTPToPrice(d);
  }

  double TotalLots() {
    return mPositions.TotalLots();
  }

  double Sum(double spread) {
    return mPositions.Sum(spread);
  }

  IPosition* At(int idx) {
    Position* p = mPositions[idx];
    return p;
  }

  int Count()
  {
    return mPositions.Count();    
  }

  void Sort()
  {
    int count = Count();
    TPOSITIONSORTER::Sort(GetPointer(mPositions), 0, count-1);
  }
};

class PositionsWapper1Factory {
public:
  static IPositions* New() {
    return new PositionsWapper1(InpMagic, SlipPage);
  }
};

#endif