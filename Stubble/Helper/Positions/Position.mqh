//+------------------------------------------------------------------+
//|                                                     Position.mqh |
//|                                                    Man-of-Christ |
//|                                                                  |
//+------------------------------------------------------------------+
#property copyright "Man-of-Christ"
#property link      ""
#property version   "1.00"
#property strict

#ifndef POSITION_MQH
#define POSITION_MQH

#include "OrderReliable.mqh"
#include "../../Interface/IPosition.mqh"
#include "../StringSettingsHelper.mqh"

class Position : public IPosition {
public:
   int _ticket;
   double _op;
   double _lot;
   int _type;
   double _profit;
   double _priceDist;
   datetime _ot;
   double _sl;
   double _tp;

   double _onePointRation;

public:
    Position() {
        _ticket = OrderTicket();
        _op = OrderOpenPrice();
        _type = OrderType();
        _lot = OrderLots();
        // https://www.mql5.com/en/forum/127064
        _profit = OrderProfit() + OrderSwap() + OrderCommission();
        _ot = OrderOpenTime();
        _sl = OrderStopLoss();
        _tp = OrderTakeProfit();
        _onePointRation = MathPow(10, Digits);

        _priceDist = (_type == OP_BUY ? PriceForClose() - _op : _op - PriceForClose()) * _onePointRation;
    }
   
    Position(int iMagic, int iType, double iLot, int iSlippage, double iSLDist=0., string iMsg="") {
        _type = iType;
        _lot = iLot;
        _op = PriceForOpen();
        _ticket = OrderSendReliable(Symbol(), _type, _lot, _op, iSlippage, 0, 0, iMsg, iMagic, 0, clrAliceBlue);
        if (iSLDist > 0) AdjSL(iSLDist);
        bool x = OrderSelect(_ticket, SELECT_BY_TICKET, MODE_TRADES);
        _op = OrderOpenPrice();
        _onePointRation = MathPow(10, Digits);

        _priceDist = (_type == OP_BUY ? PriceForClose() - _op : _op - PriceForClose()) * _onePointRation;
    }

    Position(Position& iP) {
        _ticket = iP._ticket;
        _op = iP._op;
        _lot = iP._lot;
        _type = iP._type;
        _profit = iP._profit;
        _ot = iP._ot;
        _sl = iP._sl;
        _tp = iP._tp;

        _priceDist = iP._priceDist;
    }

    Position(int iMagic, int iType, double iLot, double iPrice, int iSlippage, double iSLDist=0., string iMsg="") {
        _type = iType;
        _lot = iLot;
        _op = iPrice;
        _ticket = OrderSendReliable(Symbol(), _type, _lot, _op, iSlippage, 0, 0, iMsg, iMagic, 0, clrAliceBlue);
        if (iSLDist > 0) AdjSL(iSLDist);
        bool x = OrderSelect(_ticket, SELECT_BY_TICKET, MODE_TRADES);
        _op = OrderOpenPrice();
        _onePointRation = MathPow(10, Digits);

        _priceDist = (_type == OP_BUY ? PriceForClose() - _op : _op - PriceForClose()) * _onePointRation;
    }
   
    ~Position() {
    }
   
    double PriceForClose() {
        return ( _type==OP_BUY || _type==OP_BUYSTOP || _type==OP_BUYLIMIT ) ? Bid : Ask;
    }
   
    double PriceForOpen() {
        return ( _type==OP_BUY || _type==OP_BUYSTOP || _type==OP_BUYLIMIT ) ? Ask : Bid;
    }

    double OpenPrice() {
      return _op;
    }
   
    double SLPrice(double iSlDist) {
        return ( _type==OP_BUY || _type==OP_BUYSTOP || _type==OP_BUYLIMIT ) ? (PriceForClose() - iSlDist) : (PriceForClose() + iSlDist);
    }

    double TPPrice(double iTpDist) {
        return ( _type==OP_BUY || _type==OP_BUYSTOP || _type==OP_BUYLIMIT ) ? (PriceForClose() + iTpDist) : (PriceForClose() - iTpDist);
    }
   
    Position* AdjSL(double iSLDist) {
        double sl = SLPrice(iSLDist);
        bool x = OrderModifyReliable(_ticket, _op, sl, _tp, 0);
        if (x) _sl = sl;
        Status(__FUNCTION__);
        return GetPointer(this);
    }

    Position* AdjSLToPrice(double iSLPrice) {
        bool x = OrderModifyReliable(_ticket, _op, iSLPrice, _tp, 0);
        if (x) _sl = iSLPrice;
        Status(__FUNCTION__);
        return GetPointer(this);
    }

    Position* AdjTP(double iTPDist) {
        
        double tp = TPPrice(iTPDist);
        bool x = OrderModifyReliable(_ticket, _op, _sl, tp, 0);
        if (x) _tp = tp;
        Status(__FUNCTION__);
        return GetPointer(this);
    }

    Position* AdjTPToPrice(double iTPPrice) {
        bool x = OrderModifyReliable(_ticket, _op, _sl, iTPPrice, 0);
        if (x) _tp = iTPPrice;
        Status(__FUNCTION__);
        return GetPointer(this);
    }
   
    Position* RemoveSL() {
        bool x = OrderModifyReliable(_ticket, _op, 0, _tp, 0);
        Status(__FUNCTION__);
        return GetPointer(this);
    }

    Position* SLToDistAwayFromOpen(const double d) {
        double sl = _op + (_type==OP_BUY ? d : -d);
        bool x = OrderModifyReliable(_ticket, _op, sl, _tp, 0);
        if (x) _sl = sl;
        Status(__FUNCTION__);
        return GetPointer(this);
    }
   
    bool TrailSLReq(double iSLDist) {
        double d = 0.;
        
        if (_sl<=0.)
            d = (( _type==OP_BUY || _type==OP_BUYSTOP || _type==OP_BUYLIMIT ) ? (PriceForClose() - _op) : (_op - PriceForClose()));
        else
            d = (( _type==OP_BUY || _type==OP_BUYSTOP || _type==OP_BUYLIMIT ) ? (PriceForClose() - _sl) : (_sl - PriceForClose()));

        return (d > iSLDist);
    }
   
    string Descrip() {
        return STRHLPR::TypeToStr(_type);
    };
   
    void Close() {
        bool x = OrderSelect(_ticket, SELECT_BY_TICKET, MODE_TRADES);
        int ot = OrderType();
        if (ot==OP_BUYSTOP||ot==OP_BUYLIMIT||ot==OP_SELLSTOP||ot==OP_SELLLIMIT) {
          x = OrderDelete(OrderTicket());
        }
        else {
          x = OrderCloseReliable(OrderTicket(), OrderLots(), PriceForClose(), 3, clrAzure);
        }
    };
   
   datetime DiffFrOpenTime() {
        datetime t = TimeLocal();
        return t - _ot;
   };
   
   void Status(string x) {
        // StatusWriterFactory::Singleton().Trace(StringFormat("%s %s--L %G O %G SL %G TP %G", x, Descrip(), _lot, _op, _sl, _tp));
   };
};

#endif