//+------------------------------------------------------------------+
//|                                                     Iterator.mqh |
//|                                                    Man-of-Christ |
//|                                                                  |
//+------------------------------------------------------------------+
#property copyright "Man-of-Christ"
#property link      ""
#property version   "1.00"
#property strict

#ifndef ITERATOR_MQH
#define ITERATOR_MQH

template<typename T, typename deletable>
interface Iterator {
    void      Next();
    T         Current() const;
    bool      End() const;
    bool      Last() const;
    bool      Set(T value);  // replace current value in target collection
};

#endif