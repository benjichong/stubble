//+------------------------------------------------------------------+
//|                                            ChartStatusWriter.mqh |
//|                                                    Man-of-Christ |
//|                                                                  |
//+------------------------------------------------------------------+
#property copyright "Man-of-Christ"
#property link      ""
#property version   "1.00"
#property strict

#ifndef NULLSTATUSWRITER_MQH
#define NULLSTATUSWRITER_MQH

#include "../../Interface/IStatusWriter.mqh"
#include "../Dictionary.mqh"
#include "../LinkedList.mqh"

#define MAXTRACELEN 20
#define MAXLINELEN 150
#define XOFFSET 10
#define YOFFSET 2
#define TSTATUS Dictionary<string, NotDeletable<string>, string, NotDeletable<string>, DefaultComparator<string>, StringInvalidValue>
#define TTRACE LinkedList<string, NotDeletable<string>>
#define TKEY LinkedList<string, NotDeletable<string>>
#define TVALUE LinkedList<string, NotDeletable<string>>
#define TTRACEITERATOR Iterator<string, NotDeletable<string>>

class ChartStatusWriter : public IStatusWriter {
  TSTATUS _status;
  TTRACE _trace;
  string _xbuffer, _ybuffer;

  string XBuffer() {
    string ret = "";
    for(int i=0; i<XOFFSET; ++i) {
      ret += " ";
    }
    return ret;
  }

  string YBuffer() {
    string ret = "";
    for(int i=0; i<YOFFSET; ++i) {
      ret += "\n";
    }
    return ret;
  }

  string StatusComposed() {
    TKEY keys;
    TVALUE values;
    _status.All(keys, values);
    string curr="", line = "", ret = "";
    int len = keys.Count();
    for(int i=0; i<len; ++i) {
      curr = keys[i] + "[ " + values[i] + ( i<len-1 ? " ] " : " ]");
      if (StringLen(line) + StringLen(curr) > MAXLINELEN) {
        ret += _xbuffer + line + "\n";
        line = curr;
      }
      else {
        line += curr;
      }
    }
    ret += _xbuffer + line + "\n";
    return ret;
  }

  string TraceComposed() {
    string ret = "";
    if (_trace.Count()<=0) return ret;
    for(
      AutoDeletor<TTRACEITERATOR> iter = _trace.GetIterator();
      !iter.Ref().End();
      iter.Ref().Next()
    ) {
      ret += _xbuffer +
             StringSubstr(iter.Ref().Current(), 0, MAXLINELEN) + 
             ( iter.Ref().Last() ? "" : "\n" );
    }
    return ret;
  }

public:
  void Status(string key, string value) {
    _status.Set(key, value);
  }

  void Trace(string x) {
    _trace.AddFront(TimeToString(TimeCurrent()) + " | " + x);
    while (_trace.Count() > MAXTRACELEN)
      _trace.DeleteLast(); 
  }

  void Debug(string x) {
    Trace("dbg - " + x);
  }

  void Render() {
    string finalOut = _ybuffer + StatusComposed() + _xbuffer + "----------\n" + TraceComposed();
    Comment(finalOut);
  }

  ChartStatusWriter() {
      _xbuffer = XBuffer();
      _ybuffer = YBuffer();
  }

  static IStatusWriter* Ref() {
    static ChartStatusWriter s();
    IStatusWriter* p = GetPointer(s);
    return p;
  }
};