//+------------------------------------------------------------------+
//|                                             NullStatusWriter.mqh |
//|                                                    Man-of-Christ |
//|                                                                  |
//+------------------------------------------------------------------+
#property copyright "Man-of-Christ"
#property link      ""
#property version   "1.00"
#property strict

#ifndef NULLSTATUSWRITER_MQH
#define NULLSTATUSWRITER_MQH

#include "../../Interface/IStatusWriter.mqh"

class NullStatusWriter : public IStatusWriter {
public:
  void Status(string key, string value) {}
  void Trace(string x) {}
  void Debug(string x) {}
  void Render() {}


  static IStatusWriter* Ref() {
    static NullStatusWriter s();
    return GetPointer(s);
  }
};

#endif