//+------------------------------------------------------------------+
//|                                          StatusWriterFactory.mqh |
//|                                                    Man-of-Christ |
//|                                                                  |
//+------------------------------------------------------------------+
#property copyright "Man-of-Christ"
#property link      ""
#property version   "1.00"
#property strict

#ifndef STATUSWRITERFACTORY_MQH
#define STATUSWRITERFACTORY_MQH

#include "ChartStatusWriter.mqh"

class StatusWriterFactory {
public:
  static IStatusWriter* Singleton() {
    return ChartStatusWriter::Ref();
  }
};

#endif