//+------------------------------------------------------------------+
//|                                                  AutoDeletor.mqh |
//|                                                    Man-of-Christ |
//|                                                                  |
//+------------------------------------------------------------------+
#property copyright "Man-of-Christ"
#property link      ""
#property version   "1.00"
#property strict

#ifndef MATHHELPER_MQH
#define MATHHELPER_MQH

double MathRound(double x, double m) { return m * MathRound(x / m); }
double MathFloor(double x, double m) { return m * MathFloor(x / m); }
double MathCeil (double x, double m) { return m * MathCeil(x / m); }

#endif