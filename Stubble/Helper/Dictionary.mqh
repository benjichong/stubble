//+------------------------------------------------------------------+
//|                                                   Dictionary.mqh |
//|                                                    Man-of-Christ |
//|                                                                  |
//+------------------------------------------------------------------+
#property copyright "Man-of-Christ"
#property link      ""
#property version   "1.00"
#property strict

#ifndef DICTIONARY_MQH
#define DICTIONARY_MQH

template<typename T>
class DefaultComparator {
public:
    static bool Act(T inA, T inB) {
        return inA == inB;
    }
};

class StringInvalidValue {
public:
  static string Invalid() {
    return "";
  }
};

template<typename kT, typename kDeletable, typename T, typename deletable>
class DictionaryNode {
public:
    kT key;
    T value;
    DictionaryNode(kT k, T v) {
        key=k; value=v;
    }
    ~DictionaryNode() {
        kDeletable::Act(key);
        deletable::Act(value);
    }
};

template<
  typename kT, 
  typename kDeletable, 
  typename T, 
  typename TDeletable, 
  typename kComparator,
  typename TInvalid
>
class Dictionary {
    LinkedList<
        DictionaryNode<kT, kDeletable, T, TDeletable>*, 
        Deletable<DictionaryNode<kT, kDeletable, T, TDeletable>*>
    > dict;

public:
    void Set(kT k, T& value) {
        DictionaryNode<kT, kDeletable, T, TDeletable>* found = Find(k);
        if (found == NULL) {
            dict.Add(new DictionaryNode<kT, kDeletable, T, TDeletable>(k ,value));
        }
        else
            found.value = value;
    }

    void Delete(kT k) {
        for (
            AutoDeletor<
                ListIterator<
                    DictionaryNode<kT, kDeletable, T, TDeletable>*, 
                    Deletable<DictionaryNode<kT, kDeletable, T, TDeletable>*>
                >
            > iter = dict.GetIterator();
            !iter.Ref().End();
            iter.Ref().Next()
        ) {
            if (iter.Ref().Current().key == k) {
                iter.Ref().Delete(); 
                return;
            }
        }
    }

    T operator[](const kT x) {
        DictionaryNode<kT, kDeletable, T, TDeletable>* node = Find(x);
        if (node == NULL) return TInvalid::Invalid();
        return node.value;
    }

    void All(LinkedList<kT, kDeletable>& keys, LinkedList<T, TDeletable>& values) {
      if (dict.Count()<=0) return;
      for (
        AutoDeletor<
          ListIterator<
            DictionaryNode<kT, kDeletable, T, TDeletable>*, 
            Deletable<DictionaryNode<kT, kDeletable, T, TDeletable>*>
          >
        > iter = dict.GetIterator();
        !iter.Ref().End();
        iter.Ref().Next()
      ) {
        keys.Add(iter.Ref().Current().key);
        values.Add(iter.Ref().Current().value);
      }
    } 
protected:
    DictionaryNode<kT, kDeletable, T, TDeletable>* Find(kT k) {
        if (dict.Count()<=0) return NULL;
        for (
            AutoDeletor<
                ListIterator<
                    DictionaryNode<kT, kDeletable, T, TDeletable>*, 
                    Deletable<DictionaryNode<kT, kDeletable, T, TDeletable>*>
                >
            > iter = dict.GetIterator();
            !iter.Ref().End();
            iter.Ref().Next()
        ) {
            // found key in dict
            if (kComparator::Act(iter.Ref().Current().key, k)) return iter.Ref().Current();
        }
        return NULL;
    }
};

#endif