//+------------------------------------------------------------------+
//|                                                 LotAllocator.mqh |
//|                                                    Man-of-Christ |
//|                                                                  |
//+------------------------------------------------------------------+
#property copyright "Man-of-Christ"
#property link      ""
#property version   "1.00"
#property strict

#ifndef LOTALLOCATOR_MQH
#define LOTALLOCATOR_MQH

#include "../Globals.mqh";
#include "../Interface/ILotAllocator.mqh"
#include "MathHelper.mqh"
#include "Status/StatusWriterFactory.mqh"

class LotAllocator : public ILotAllocator {
private:
  IStatusWriter* mStatus;
public:
  double AllocateBaseLotsize() {
    double volume_min = SymbolInfoDouble(Symbol(), SYMBOL_VOLUME_MIN);
    double volume_max = SymbolInfoDouble(Symbol(), SYMBOL_VOLUME_MAX);
    double volume_step = SymbolInfoDouble(Symbol(), SYMBOL_VOLUME_STEP);
    double lots = SymbolInfoDouble(Symbol(), SYMBOL_VOLUME_MIN);

    if (InpLotMode == LOT_MODE_FIXED) lots = InpFixedLot;
    else if (InpLotMode == LOT_MODE_PERCENT) {
      double risk_balance = InpPercentLot * AccountBalance() / 100.0;
      double margin_required = MarketInfo(Symbol(), MODE_MARGINREQUIRED);
      lots = MathRound(risk_balance / margin_required, volume_step);
      if (lots < volume_min) lots = volume_min;
      if (lots > volume_max) lots = volume_max;
    }
    string xx = __FUNCTION__ + " " + DBLSTRX(lots);
    return lots;
  }

  double Allocate(
    double inLots,
    int total_orders_count, // buy + sell counts
    bool long_condition,
    bool short_condition,
    double buyer_lots, 
    double seller_lots
  ) {
    double Allocate(
    double inLots,
    int total_orders_count, // buy + sell counts
    bool long_condition,
    bool short_condition,
    double buyer_lots, 
    double seller_lots
  ) {
    return inLots;
  }

  LotAllocator() :
    mStatus(StatusWriterFactory::Singleton())
  {}
};

#endif