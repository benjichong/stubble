//+------------------------------------------------------------------+
//|                                          StringSettingHelper.mqh |
//|                                                    Man-Of-Christ |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+

#property copyright "Man-Of-Christ"
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict

#ifndef STRINGSETTINGHELPER_MQH
#define STRINGSETTINGHELPER_MQH

#define STRHLPR CStringSettingHelper

#define ZPRICESTR(p) DoubleToStr( p, 6 )
#define TFSTR(s) #s+"["+CStringSettingHelper::TrueFalse(s)+"]^"
#define TFSTRX(s) #s+"["+CStringSettingHelper::TrueFalse(s)+"]"
#define TYPESTR(s) #s+"["+CStringSettingHelper::TypeToStr(s)+"]^"
#define TYPESTRX(s) #s+"["+CStringSettingHelper::TypeToStr(s)+"]"
#define DBLSTR(s) #s+"["+DoubleToStr(s,4)+"]^"
#define DBLSTRX(s) #s+"["+DoubleToStr(s,4)+"]"
#define INTSTR(s) #s+"["+IntegerToString(s)+"]^"
#define INTSTRX(s) #s+"["+IntegerToString(s)+"]"
#define STRSTR(s) #s+"["+s+"]^"
#define STRSTRX(s) #s+"["+s+"]"
#define ENUMSTR(s) #s+"["+EnumToString(s)+"]^"
#define ENUMSTRX(s) #s+"["+EnumToString(s)+"]"
#define OTYPSTR(s) #s+"["+(s==OP_BUY?"BUY":"SELL")+"]^"
#define OTYPSTRX(s) #s+"["+(s==OP_BUY?"BUY":(s==OP_SELL?"SELL":"INVLD"))+"]"
#define TITLESTR(s) #s+"\n^"

#define PUSH2ARRAY(arr, val) \
    { \
        const int l = ArraySize( arr ); \
        ArrayResize( arr, l+1 ); \
        arr[l] = val; \
    }

#define POPHEAD(arr) \
    {\
        const int l = ArraySize( arr ); \
        for(int i=1; i<l; ++i) { \
            arr[i-1] = arr[i]; \
        } \
    }

#define STREQ(s0, s1) \
    (StringCompare(s0, s1) == 0)

class CStringSettingHelper {
public:
    static string TrueFalse(const bool iTF) {
        return (iTF ? "O":"X");
    }

    static string TypeToStr(int itype) {
        switch(itype) {
        case OP_BUY: return "OP_BUY";
        case OP_SELL: return "OP_SELL";
        case OP_BUYLIMIT: return "OP_BUYLIMIT";
        case OP_SELLLIMIT: return "OP_SELLLIMIT";
        case OP_BUYSTOP: return "OP_BUYSTOP";
        case OP_SELLSTOP: return "OP_SELLSTOP";
        }
        return "";
    }

    static bool EqNoCase(const string p, const string q) {
        return (StringCompare(p,q,false)==0);
    }

	static string Trimx(const string s) {
        string r = s;
        StringToUpper(r);
        r = StringTrimLeft(r);
        r = StringTrimRight(r);
        return r;
    }

    static string Trim(const string s) {
        string r = s;
        r = StringTrimLeft(r);
        r = StringTrimRight(r);
        return r;
    }

    static string ArrStr(string &arr[]) {
        string o = "";
        int s = ArraySize(arr);
        for(int i=0; i<s; ++i)
        {
            o += ((i>0) ? ", " : "") + arr[i];
        }
        return o;
    }

    static int FindLastOf(string targ, string substr, int pos=0) {
        int newpos = StringFind(targ, substr, pos);
        if (newpos<=-1) return pos-1;
        return FindLastOf(targ, substr, newpos+1);
    }
};

#endif