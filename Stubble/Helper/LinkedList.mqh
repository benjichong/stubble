//+------------------------------------------------------------------+
//|                                                    Positions.mqh |
//|                                                    Man-of-Christ |
//|                                                                  |
//+------------------------------------------------------------------+
#property copyright "Man-of-Christ"
#property link      ""
#property version   "1.00"
#property strict

#ifndef LINKEDLIST_MQH
#define LINKEDLIST_MQH

#include "Iterator.mqh"

template<typename T>
class Deletable {
public:
    static void Act(T t) {
        delete t;
    }
};

template<typename T>
class NotDeletable {
public:
    static void Act(T t) {
        return;
    }
};

template<typename T, typename deletable>
class LinkedNode {
    T mT;
    LinkedNode<T, deletable>* mPrev;
    LinkedNode<T, deletable>* mNext;
public:
    LinkedNode(T t): mT(t), mPrev(NULL), mNext(NULL) {}
    ~LinkedNode() {
        deletable::Act(mT);
    }

    void DeleteAllNext() {
        if (mNext) {
            mNext.DeleteAllNext();
        }
        if (mNext) {
            delete mNext;
        }
    }

    void DeleteAt(int i) {
        if (i==1) {
            if (mNext) {
                LinkedNode<T, deletable>* toBeRemoved = mNext;
                if (mNext.mNext) {
                    toBeRemoved.mNext.mPrev = GetPointer(this);
                    mNext = toBeRemoved.mNext;
                }
                else {
                    mNext = NULL;
                }
                delete toBeRemoved;
            }
            return;
        }
        mNext.DeleteAt(i-1);
    }

    void Delete(T t) {
        if (mNext && mNext.mT == t) {
            LinkedNode<T, deletable>* toBeRemoved = mNext;
            if (mNext.mNext) {
                toBeRemoved.mNext.mPrev = GetPointer(this);
                mNext = toBeRemoved.mNext;
            }
            else {
                mNext = NULL;
            }
            delete toBeRemoved;
        }
        else
        if (mNext) {
            mNext.Delete(t);
        }
    }

    LinkedNode* GetAt(int i) {
        if (i>0) {
            return mNext.GetAt(i-1);
        }
        if (i==0) {
            return GetPointer(this);
        }
        return NULL;
    }

    LinkedNode* GetLast() {
        if (mNext == NULL) {
            return GetPointer(this);
        }
        return mNext.GetLast();
    }

    void Prev(LinkedNode<T, deletable>* p) {
        if (mPrev) return;
        mPrev = p;
        mPrev.mNext = GetPointer(this);
    }

    LinkedNode<T, deletable>* Prev() {
        return mPrev;
    }

    void Next(LinkedNode<T, deletable>* n) {
        if (mNext) return;
        mNext = n;
        mNext.mPrev = GetPointer(this);
    }

    LinkedNode<T, deletable>* Next() {
        return mNext;
    }

    T GetElem() {
        return mT;
    }

    T GetElemAt(const int x) {
        return GetAt(x).GetElem();
    }

    bool Set(T t) {
        deletable::Act(mT);
        mT = t;
        return true;
    }
    
    bool Swap(T t) {
        mT = t;
        return true;
    }

    int Count(int x=1) {
        if (mNext==NULL) {
            return x;
        }
        return mNext.Count(x+1);
    }

    int Find(T t, int i=0) {
        if (mT == t) { return i; }
        if (mNext) return mNext.Find(t, i+1);
        return -1;
    }
};

template<typename T, typename deletable>
class ListIterator: public Iterator<T, deletable> {
private:
    LinkedNode<T, deletable>*m_p;
    LinkedNode<T, deletable>*m_end;
    bool m_ended;
public:
    ListIterator(LinkedNode<T, deletable>*head){
        if (head == NULL) {
            m_ended = true;
            m_p = NULL;
            m_end = NULL;
            return;
        }
        m_ended = false;
        m_p = head;
        m_end = head.GetLast();
    }
    bool End() const {return m_ended;}
    void Next() {if (m_p != m_end) m_p=m_p.Next(); else m_ended = true;}
    bool Last() const {return m_p == m_end;}
    T Current() const {return m_p.GetElem();}
    bool Set(T value) { return m_p.Set(value);}
    void Delete() {
        if (m_p.Prev() != NULL) {
            m_p.Prev().Next(m_p.Next());
        }
        if (m_p.Next() != NULL) {
            m_p.Next().Prev(m_p.Prev());
        }
        delete m_p;
    }
};

template<typename T, typename deletable>
class LinkedList {
    LinkedNode<T, deletable>* m_head;
public:
    LinkedList() {
        m_head = NULL;
    }

    ~LinkedList() {
        DeleteAll();
    }

    void DeleteAll() {
        if (m_head == NULL) {
            return;
        }
        m_head.DeleteAllNext();
        delete m_head;
        m_head = NULL;
    }

    void DeleteAt(int i) {
        if (i<0) {
            return;
        }
        if (i==0) {
            LinkedNode<T, deletable>* toBeRemoved = m_head;
            m_head = toBeRemoved.Next();
            delete toBeRemoved;
        }
        else {
            m_head.DeleteAt(i-1);
        }
    }

    void Delete(T t) {
        if (m_head.GetElem()==t) {
            LinkedNode<T, deletable>* toBeRemoved = m_head;
            m_head = toBeRemoved.Next();
            delete toBeRemoved;
        }
        else {
            m_head.Delete(t);
        }
    }

    void DeleteLast() {
        int x = m_head.Count();
        m_head.DeleteAt(x-1);
    }

    T Add(T t) {
        LinkedNode<T, deletable>* newNode = new LinkedNode<T, deletable>(t);
        if (m_head == NULL) {
            m_head = newNode;
        }
        else {
            m_head.GetLast().Next(newNode);
        }
        return newNode.GetElemAt(0);
    }

    T AddFront(T t) {
        LinkedNode<T, deletable>* newNode = new LinkedNode<T, deletable>(t);
        if (m_head == NULL) {
            m_head = newNode;
        }
        else {
          LinkedNode<T, deletable>* temp = m_head;
          m_head = newNode;
          m_head.Next(temp);
        }
        return m_head.GetElemAt(0);
    }

    T SetAt(int i, T t) {
        if (i<0 || i>Count()) return t;
        m_head.GetAt(i).Set(t);
        return t;
    }
    
    T SwapAt(int i, T t) {
        if (i<0 || i>Count()) return t;
        m_head.GetAt(i).Swap(t);
        return t;
    }

    int Count() {
        if (m_head==NULL) return 0;
        return m_head.Count();
    }

    int Find(T t) {
        return m_head.Find(t);
    }

    T operator[](const string x) {
        return At(x);
    }

    T operator[](int i) {
        return m_head.GetElemAt(i);
    }

    T At(const string x) {
      string y = x;
      StringToLower(y);
      if (y == "first") return m_head.GetElemAt(0);
      if (y == "last") return m_head.GetLast().GetElem();
      return NULL;
    }

    T At(int i) {
      return m_head.GetElemAt(i);
    }

    Iterator<T, deletable>* GetIterator() {
        if (m_head == NULL) {
            return NULL;
        }
        return new ListIterator<T, deletable>(m_head);
    }
};

#endif