//+------------------------------------------------------------------+
//|                                                 SpreadFilter.mqh |
//|                                                    Man-of-Christ |
//|                                                                  |
//+------------------------------------------------------------------+
#property copyright "Man-of-Christ"
#property link      ""
#property version   "1.00"
#property strict

#ifndef SPREADFILTER_MQH
#define SPREADFILTER_MQH

#include "../../Globals.mqh"
#include "../../Interface/IFilter.mqh"
#include "../../Interface/IFilterManager.mqh"
#include "../Status/StatusWriterFactory.mqh"
#include "../../Interface/IPositions.mqh"

class SpreadFilter: public IFilter {
private:
  IStatusWriter* mStatus;
public:
  bool Triggered() {
    double spread = MarketInfo(Symbol(), MODE_SPREAD);
    if (spread > InpMaxSpread) {
      mStatus.Trace("MaxSpread ON - no action.");
      return true;
    }
    return false;
  }

  SpreadFilter() :
    mStatus(StatusWriterFactory::Singleton())
  {}
};

#endif