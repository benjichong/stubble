//+------------------------------------------------------------------+
//|                                         OptimizeSanityFilter.mqh |
//|                                                    Man-of-Christ |
//|                                                                  |
//+------------------------------------------------------------------+
#property copyright "Man-of-Christ"
#property link      ""
#property version   "1.00"
#property strict

#ifndef OPTIMIZESANITY_MQH
#define OPTIMIZESANITY_MQH

#include "../../Globals.mqh"
#include "../../Interface/IFilter.mqh"
#include "../../Interface/IFilterManager.mqh"
#include "../Status/StatusWriterFactory.mqh"

class OptimizationSanityFilter: public IFilter {
  IStatusWriter* mStatus;
public:
  OptimizationSanityFilter():
    mStatus(StatusWriterFactory::Singleton())
  {
  }

  bool Triggered() {
    if (!IsOptimization()) return false;
    mStatus.Trace(__FUNCTION__ + " is not working.");
    return false;
  }
};

#endif