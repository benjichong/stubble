//+------------------------------------------------------------------+
//|                                                FilterManager.mqh |
//|                                                    Man-of-Christ |
//|                                                                  |
//+------------------------------------------------------------------+
#property copyright "Man-of-Christ"
#property link      ""
#property version   "1.00"
#property strict

#ifndef FILTERMANAGER_MQH
#define FILTERMANAGER_MQH

#include "../../Interface/IFilterManager.mqh"
#include "../../Interface/IFilter.mqh"
#include "NewCandleFilter.mqh"
#include "SpreadFilter.mqh"
#include "OptimizationSanityFilter.mqh"
#include "../Status/StatusWriterFactory.mqh"

class FilterManager : public IFilterManager {
    LinkedList<IFilter*, Deletable<IFilter*>> mFilters; 
    IStatusWriter* mStatus;
public:
  bool Triggered() {
    for(
      AutoDeletor<ListIterator<IFilter*, Deletable<IFilter*>>> iter = mFilters.GetIterator();
      !iter.Ref().End();
      iter.Ref().Next()
    ) {
      //mStatus.Debug("FilterManager runs.");
      if (iter.Ref().Current().Triggered()) {
        //mStatus.Trace("FilterManager is blocking trades.");
        return true;
      }
    }

    // mStatus.Trace("FilterManager - OK to trade.");
    return false;
  }

  void Insert(IFilter* filter)
  {
    mFilters.Add(filter);
  }

  FilterManager() :
    mStatus(StatusWriterFactory::Singleton())
  {
    mFilters.Add(new SpreadFilter());
    mFilters.Add(new NewCandleFilter());
    mFilters.Add(new OptimizationSanityFilter());
  }
};

#endif