//+------------------------------------------------------------------+
//|                                              NewCandleFilter.mqh |
//|                                                    Man-of-Christ |
//|                                                                  |
//+------------------------------------------------------------------+
#property copyright "Man-of-Christ"
#property link      ""
#property version   "1.00"
#property strict

#ifndef NEWCANDLEFILTER_MQH
#define NEWCANDLEFILTER_MQH

class NewCandleFilter: public IFilter {
private:
  datetime m_currCandleDateTime;
  IStatusWriter* mStatus;
public:
  bool Triggered() {
    if (!InpOpenOneCandle) return false;
    datetime testCurrDateTime = iTime(NULL, InpTimeframeBarOpen, 0);
    //mStatus.Debug("NewCandleFilter working " + TimeToString(testCurrDateTime) + " vs " + TimeToString(m_currCandleDateTime));
    if (m_currCandleDateTime != testCurrDateTime) {
      m_currCandleDateTime = testCurrDateTime;
      //mStatus.Debug("Diff Candle, not triggered");
      return false;
    }

    //mStatus.Debug("Same Candle, triggered");
    return true;
  }

  NewCandleFilter():
    mStatus(StatusWriterFactory::Singleton())
  {
    m_currCandleDateTime = 0;
  }
};

#endif