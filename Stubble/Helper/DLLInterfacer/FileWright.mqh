//+------------------------------------------------------------------+
//|                                                   FileWright.mqh |
//|                                                    Man-of-Christ |
//|                           https://www.mql5.com/en/users/bjhchong |
//+------------------------------------------------------------------+
#property copyright "Man-of-Christ"
#property link      "https://www.mql5.com/en/users/bjhchong"
#property strict

#ifndef FILEWRIGHT_MQH
#define FILEWRIGHT_MQH

#import "fileWright.dll"
    void ToFile(uchar& filename[], uchar &arr[], const int len, bool overwrite);
    void FromFile(uchar& filename[], int &lastPos, int &fileLen, uchar& buffer[], const int len);
#import

class FileWright {
public:
    static void D(int level, string in) {
        if (level > InpDebugLevel) return;
        W(InputRunningLogs, in+"\n", false); 
    }

    static void W(string filename, string in, bool overwrite=false) {
        uchar arr[], fn[];
        StringToCharArray(filename, fn, 0, -1, CP_UTF8);
        StringToCharArray(in, arr, 0, -1, CP_UTF8); 
        ToFile(fn, arr, StringLen(in), overwrite);
    }

    static void R(string filename, string &out) {
        out = "";
        uchar arr[], fn[];
        int lastPos=0, fileLen=0;
        StringToCharArray(filename, fn, 0, StringLen(filename));
        ArrayResize(arr, 1024);

        FromFile(fn, lastPos, fileLen, arr, 1024);
        out += CharArrayToString(arr);
        for (; lastPos>0 && lastPos<fileLen;) {
            FromFile(fn, lastPos, fileLen, arr, 1024);
            out += CharArrayToString(arr);
        }
    }
};

#endif