//+------------------------------------------------------------------+
//|                                                   HttpClient.mq4 |
//|                                                    Man-of-Christ |
//|                                                                  |
//+------------------------------------------------------------------+
#property copyright "Man-of-Christ"
#property link      ""
#property version   "1.00"
#property strict

#ifndef HTTPCLIENT_MQH
#define HTTPCLIENT_MQH

#import "HttpClientDLL.dll"
  // this has additional DLL dependencies.
  void httpPost(const uchar& url[], const uchar& reqJson[], uchar& respStr[], const int respStringLen); 

  void httpGet(const uchar& url[], const uchar& queryStr[], uchar& respStr[], const int respStringLen); 
#import

class HttpClient {
public:
  static string Post(string url, string reqJson) {
    uchar urlArr[], reqArr[];
    StringToCharArray(url, urlArr, 0, -1, CP_UTF8);
    StringToCharArray(reqJson, reqArr, 0, -1, CP_UTF8); 

    const int rcxSize = 1024;
    uchar rcx[];
    ArrayResize(rcx, rcxSize);
    // ArrayFill(rcx, 0, rcxSize, 0);

    httpPost(urlArr, reqArr, rcx, rcxSize);

    string out = "";
    out += CharArrayToString(rcx);
    return out;
  }

  static string Get(string url, string queryStr) {
    uchar urlArr[], qArr[];
    StringToCharArray(url, urlArr, 0, -1, CP_UTF8);
    StringToCharArray(queryStr, qArr, 0, -1, CP_UTF8); 

    const int rcxSize = 1024;
    uchar rcx[];
    ArrayResize(rcx, rcxSize);
    // ArrayFill(rcx, 0, rcxSize, 0);

    httpGet(urlArr, qArr, rcx, rcxSize);

    string out = "";
    out += CharArrayToString(rcx);
    return out;
  }
};

#endif