//+------------------------------------------------------------------+
//|                                               KillAtDrawDown.mqh |
//|                                                    Man-of-Christ |
//|                                                                  |
//+------------------------------------------------------------------+
#property copyright "Man-of-Christ"
#property link      ""
#property version   "1.00"
#property strict

#ifndef KILLATDRAWDOWN_MQH
#define KILLATDRAWDOWN_MQH

#include "../../Globals.mqh"

class KillAtDrawDown : public IKillAtDrawDown {
private:
  IStatusWriter* mStatus;
  bool mTriggered;

public:
  KillAtDrawDown():
    mStatus(StatusWriterFactory::Singleton()),
    mTriggered(false)
  {
  }

  void TryClosePositions(IPositions* positions) {
    double profit = positions.Profit();
    if (InpMaxDrawdown > 0. && profit <= -InpMaxDrawdown) {
      mTriggered = true;
      mStatus.Trace("====================================");
      // mStatus.Trace("REMOVE EA, REINSERT TO RESUME");
      // mStatus.Trace("TRADING HAS STOPPED");
      mStatus.Trace("MAX DD OF -" + DoubleToStr(InpMaxDrawdown) + " REACHED - KILLED ALL POSITION");
      mStatus.Trace("====================================");
      mStatus.Render();
      positions.CloseAndRemoveAll();
    }
  }
};


#endif