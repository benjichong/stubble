# Overview
+ This is a demo MQL4 project that demonstrates how I structure a EA project.
+ It is definitely __NOT READY FOR DEPLOYMENT__.
+ It is however, compilable.

# Features that is usable.
+ Position manager: IPositions
+ OnCandle filtering: IFilter, IFilterManager
  + filters OnTimer/OnTick to be actioned on only when a new candle starts
+ Spread filtering: IFiler, IFilterManager
  - filters OnTick/OnTimer to be actioned on only when spread is within required threshold.
+ KillAtDrawDown: IKillAtDrawDown
  - safety check
+ LinkList 
  - Template based linked list implementation.
+ Dictionary 
  - Template based dictionary implementation
+ QuickSort 
  - Quick sort for linked list.
+ AutoDeletor
  - Simple auto-pointer implementation.

# Why the interfaces?
+ I attempted to use dependency injection and factory methods to reduce inter-objects dependencies.