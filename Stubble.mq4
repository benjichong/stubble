//+------------------------------------------------------------------+
//|                                                      Stubble.mq4 |
//|                                                    Man-of-Christ |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+

#property copyright "Copyright 2018, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "5.50"
#property description "Stubble"
#property description "================================================================"
#property strict

#include "Stubble/Core/StubbleCore.mqh"
#include "Stubble/Helper/Status/StatusWriterFactory.mqh"

IStatusWriter* statusWriter;

//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit() {
    statusWriter = StatusWriterFactory::Singleton();
    statusWriter.Debug("---------- EA start -----------");
    return(INIT_SUCCEEDED);
}

//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)  {
    statusWriter.Debug("---------- EA end  ------------");
}

//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()  {
    RefreshRates();
    TSTUBBLECORE::Ref().OnTick();
}

//+------------------------------------------------------------------+
//| Timer function                                                   |
//+------------------------------------------------------------------+
void OnTimer(void) {
    
}
